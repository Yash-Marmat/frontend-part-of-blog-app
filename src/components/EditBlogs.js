import React, { Component } from "react";
import { connect } from "react-redux";
import { updateBlog } from "../actions/blogs";

// a from to create blogs
export class Edits extends Component {
  handleEdit = (e) => {
    e.preventDefault();
    const newTitle = this.getTitle.value;
    const newDescription = this.getDescription.value;
    const data = {
      newTitle,
      newDescription,
    };

    this.props.updateBlog(data, this.props.cloneID);
  };

  render() {
    return (
      <div>
        <h2>Update Blog</h2>
        <form onSubmit={this.handleEdit}>
          <div>
            <label>Title</label>
            <input
              type="text"
              ref={(input) => (this.getTitle = input)}
              onChange={this.onChange}
              defaultValue={this.props.title}
            />
          </div>
          <br />
          <div>
            <label>Description</label>
            <textarea
              type="text"
              ref={(input) => (this.getDescription = input)}
              onChange={this.onChange}
              defaultValue={this.props.description}
            />
          </div>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(null, { updateBlog })(Edits);
