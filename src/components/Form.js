import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addBlogs } from "../actions/blogs";

// a from to create blogs
export class Form extends Component {
  state = {
    title: "",
    description: "",
  };

  static propTypes = {
    addBlogs: PropTypes.func.isRequired,
  };

  onChange = (e) =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  onSubmit = (e) => {
    e.preventDefault();
    const { title, description } = this.state;
    const blog = { title, description };
    this.props.addBlogs(blog); // passing argument for the payload
  };

  render() {
    const { title, description } = this.state;
    return (
      <div>
        <h2>New Blog</h2>
        <form onSubmit={this.onSubmit}>
          <div>
            <label>Title</label>
            <input
              type="text"
              name="title"
              onChange={this.onChange}
              value={title}
            />
          </div>
          <br />
          <div>
            <label>Description</label>
            <textarea
              type="text"
              name="description"
              onChange={this.onChange}
              value={description}
            />
          </div>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(null, { addBlogs })(Form);
