import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getBlogs, deleteBlogs, updateBlog } from "../actions/blogs";
import Edits from "./EditBlogs";

export class Blogs extends Component {
  state = {
    editing: false,
    cloneID: 0,
  };

  static propTypes = {
    blogs: PropTypes.array.isRequired,
    getBlogs: PropTypes.func.isRequired,
    deleteBlogs: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.getBlogs();
  }

  // toggle edit function
  handleEdits = (id) => {
    this.setState((prevState) => ({
      editing: !prevState.editing,
      cloneID: id,
    }));
  };

  // updation in blog
  handleUpdates = (item, id) => {
    this.props.updateBlog(item, id);
    console.log(id);
  };

  render() {
    console.log(this.state.editing);
    return (
      <Fragment>
        <h2>All Blogs</h2>
        {this.props.blogs.map((blog) => (
          <div key={blog.id}>
            {blog.title}
            <br />
            {blog.description}
            <button
              onClick={() => {
                this.handleEdits(blog.id);
                this.handleUpdates(blog, blog.id);
              }}
            >
              edit
            </button>
            {this.state.editing ? (
              <Edits
                title={blog.title}
                description={blog.description}
                cloneID={this.state.cloneID}
              />
            ) : (
              ""
            )}
            <button onClick={this.props.deleteBlogs.bind(this, blog.id)}>
              delete
            </button>
            <hr />
          </div>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  blogs: state.blogs.blogs, // from combine reducer and initial state of reducer
});

export default connect(mapStateToProps, { getBlogs, deleteBlogs, updateBlog })(
  Blogs
);
