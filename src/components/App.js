import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import Blogs from "./Blogs";
import store from "../store";
import Form from "./Form";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Form />
          <Blogs />
        </Fragment>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
