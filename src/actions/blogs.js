import axios from "axios";
import { GET_BLOGS, DELETE_BLOG, ADD_BLOG, UPDATE_BLOG } from "./types";

// render blogs list
export const getBlogs = () => (dispatch) => {
  axios
    .get("http://localhost:8000/api/blogs/")
    .then((res) => {
      dispatch({
        type: GET_BLOGS,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// delete blogs
export const deleteBlogs = (id) => (dispatch) => {
  axios
    .delete(`http://localhost:8000/api/blogs/${id}/`)
    .then((res) => {
      dispatch({
        type: DELETE_BLOG,
        payload: id,
      });
    })
    .catch((err) => console.log(err));
};

// create blogs
export const addBlogs = (blog) => (dispatch) => {
  axios
    .post("http://localhost:8000/api/blogs/", blog)
    .then((res) => {
      dispatch({
        type: ADD_BLOG,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// update blog
export const updateBlog = (blog, id) => (dispatch) => {
  console.log("i got", id);
  axios
    .patch(`http://localhost:8000/api/blogs/${id}/`, blog)
    .then((response) => {
      dispatch({
        type: UPDATE_BLOG,
        payload: response.data,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
