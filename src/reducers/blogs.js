import {
  DELETE_BLOG,
  GET_BLOGS,
  ADD_BLOG,
  UPDATE_BLOG,
} from "../actions/types.js";

const initialState = {
  blogs: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_BLOGS:
      return {
        ...state,
        blogs: action.payload,
      };
    case DELETE_BLOG:
      return {
        ...state,
        blogs: state.blogs.filter((blog) => blog.id !== action.payload),
      };
    case ADD_BLOG:
      return {
        ...state,
        blogs: [...state.blogs, action.payload],
      };
    case UPDATE_BLOG:
      const updates = state.blogs.map((blog) => {
        if (blog.id === action.payload.id) {
          return { ...blog, ...action.payload };
        }
        return blog;
      });
      return {
        ...state,
        blogs: updates,
      };
    default:
      return state;
  }
}
